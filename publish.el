(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Disable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "tuto-techno-guix-hpc"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["tuto-techno-guix-hpc.org"]
             :preparation-function '(set-minted-options-for-dark-background)
             :completion-function '(unset-minted-options-for-dark-background)
             :publishing-function '(org-beamer-publish-to-pdf)
             :publishing-directory "./public")))

(provide 'publish)
