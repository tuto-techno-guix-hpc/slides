# Guix and Org mode, a powerful association for building a reproducible research study

[![pipeline status](https://gitlab.inria.fr/tuto-techno-guix-hpc/slides/badges/master/pipeline.svg)](https://gitlab.inria.fr/tuto-techno-guix-hpc/slides/-/commits/master)

Reproducibility of a research study in computer science has always been a
complex matter. On one hand, building exactly the same software environment on
multiple computing platforms may be long, tedious and sometimes virtually
impossible to be done manually. On the other hand, while the experimental method
is usually explained in research studies, the instructions required to reproduce
the latter from A to Z are often missing or incomplete.

In this edition of Tutotechno, we will introduce the GNU Guix transactional
package manager [1] together with the principles of literate programming [2]
through Org mode [3, 4] and learn how we can take advantage of the association
of these tools to build a reproducible research study. The session will be
introduced with a presentation of a concrete example of a research study
conducted within an ongoing PhD thesis combining Guix to ensure the
reproducibility of the software environment with Org mode in an attempt to
maintain an exhaustive, clear and accessible description of the experiments, the
source code and procedures involved in the construction of the experimental
software environment, the execution of benchmarks as well as the gathering and
the post-processing of the results.

[Show slides](https://tuto-techno-guix-hpc.gitlabpages.inria.fr/slides/tuto-techno-guix-hpc.pdf)

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).

2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).

3. The Org Mode 9.1 Reference Manual, Dominik Carsten, 2018.

4. GNU Emacs: An extensible, customizable, free/libre text editor and more
   [https://www.gnu.org/software/emacs/](https://www.gnu.org/software/emacs/).
